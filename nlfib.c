/*********************************************************************************************************
 *                                                                                                       *
 * nlfib  - calculate a specific fibonacci number, or a range of fibonacci numbers                       *
 *                                                                                                       *
 * Copyright (C) 2019 Daniel Jenssen <daerandin@gmail.com>                                               *
 *                                                                                                       *
 * This program is free software: you can redistribute it and/or modify                                  *
 * it under the terms of the GNU General Public License as published by                                  *
 * the Free Software Foundation, either version 3 of the License, or                                     *
 * (at your option) any later version.                                                                   *
 *                                                                                                       *
 * This program is distributed in the hope that it will be useful,                                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                                        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                         *
 * GNU General Public License for more details.                                                          *
 *                                                                                                       *
 * You should have received a copy of the GNU General Public License                                     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.                                 *
 *                                                                                                       *
 *                                                                                                       *
 * Run this program with an integer as argument, or two integers for a range of fibonacci numbers        *
 *                                                                                                       *
 * This program requires at least a positive integer as argument. This is the fibonacci number           *
 * you wish to calculate. You may provide two integers, with the second larger than the first,           *
 * ths will print a range of fibonacci numbers, starting at your first number and ending at the          *
 * last.                                                                                                 *
 *                                                                                                       *
 * One command line option exists for this program:                                                      *
 *                                                                                                       *
 *      -r  range: Combine this with a single integer to get a range from 0 up to your provided          *
 *          integer. This is equivalent to running the program with two integers and the first           *
 *          integer being 0.                                                                             *
 *                                                                                                       *
 * Usage examples:                                                                                       *
 *                 nlfib 28      will print fibonacci number 28                                          *
 *                 nlfib -r 28   will print a range of fibonacci numbers from 0 to 28                    *
 *                 nlfib 0 28    produce identical results to the above example, 0 to 28                 *
 *                 nlfib 20 30   will print a range of fibonacci numbers from 20 to 30                   *
 *                                                                                                       *
 *                                                                                                       *
 *********************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <gmp.h>

void calc_fibo(unsigned long, unsigned long, bool);

int main(int argc, char **argv)
{
    char *ptr;
    bool range = false, start_argument = false, stop_argument = false;
    unsigned long start = 0, stop = 0;
    if (argc < 2) {
        printf("Error: Missing required argument.\nUsage: nlfib [INT]\n");
        exit(EXIT_FAILURE);
    }
    /* Parse the command line arguments. */
    while (argc-- > 1) {
        ptr = *(++argv);
        /* Encountering a dash means it should be a command line option, followed by a single character. */
        if (*(ptr) == '-') {
            if (*(++ptr) == 'r' && !(*(ptr + 1)))
                range = true;
            else {
                printf("Error: Unknown command line option: -%s\n", ptr);
                exit(EXIT_FAILURE);
            }
        } else {
            while (*ptr)
                if (!isdigit(*(ptr++))) {
                    printf("Error: argument is not a positive integer\n");
                    exit(EXIT_FAILURE);
                }
            if (!start_argument) {
                start = atol(*argv);
                start_argument = true;
            } else if (!stop_argument) {
                stop = atol(*argv);
                stop_argument = true;
            } else {
                printf("Error: Too many integer arguments given. Maximum two integer arguments.\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    /* Test if start is lesser than stop, which it should. */
    if (start_argument && stop_argument) {
        /* Both a start and stop INT automatically means that a range will be printed. */
        if (!range)
            range = true;
        if (start >= stop) {
            printf("Error: The provided START integer is not smaller than the provided STOP integer.\n");
            exit(EXIT_FAILURE);
        }
    } else if (start_argument && !stop_argument) {
        stop = start;
        start = 0;
    } else if (!start_argument && !stop_argument) {
        /* If we have neither start or stop, we're missing an INT argument. */
        printf("Error: Missing required INT argument.\n");
        exit(EXIT_FAILURE);
    }
    calc_fibo(start, stop, range);
    return 0;
}

void calc_fibo(unsigned long start, unsigned long stop, bool range)
{
    char *str = NULL;
    mpz_t previous, current;
    mpz_inits(previous, current, NULL);
    if (!range)
        mpz_fib_ui(current, stop);
    else {
        mpz_fib2_ui(current, previous, start);
        for (; start < stop; start++) {
            printf("%s\n", str = mpz_get_str(str, 10, current));
            free(str);
            str = NULL;
            mpz_swap(previous, current);
            mpz_add(current, current, previous);
        }
    }
    printf("%s\n", str = mpz_get_str(str, 10, current));
    free(str);
    str = NULL;
    mpz_clears(previous, current, NULL);
}
