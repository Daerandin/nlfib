# nlfib - calculate either a single fibonacci number or a range

OUT = nlfib
CC = gcc
OBJ = nlfib.o
CFLAGS = -march=native -pthread -pedantic -Wall -Werror -Wextra -fstack-protector-strong -O2


nlfib: $(OBJ)
	$(CC) $(CFLAGS) -lgmp -o $(OUT) $(OBJ)

nlfib.o: nlfib.c
	$(CC) $(CFLAGS) -lgmp -c -o $@ $<

clean:
	$(RM) $(OUT) $(OBJ)

.PHONY: clean
