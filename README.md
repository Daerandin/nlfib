NAME
====
nlfib - calculate a single fibonacci number, or a range of fibonacci numbers

SYNOPSIS
========
Usage:

nlfib -r START STOP

DEPENDENCIES
============
gmp (gnu multiple precision arithmetic library)

DESCRIPTION
===========
nlfib requires at least one argument. If you only provide a single argument to the program, it must be a positive integer, n. The program will then print out the nth fibonacci number. You may also provide a second integer argument. The second one must be of greater value than the first, and nlfib will then print out the full range of fibonacci numbers between these numbers inclusive.

If you simply want to see all fibonacci numbers from 0 and up to a certain integer, use the -r option along with a single integer.

The program calculates fibonacci numbers fairly quickly as it uses the gmp libraries functions for the task. There are some limits to the program. The increasingly large numbers will eventually make gmp overflow. This is an issue with the gmp library, although it should not be an issue as you need to reach ridiculously large fibonacci numbers for that to happen.

USAGE EXAMPLES
==============
nlfib 28      will print fibonacci number 28

nlfib -r 28   will print a range of fibonacci numbers from 0 to 28

nlfib 0 28    produce identical results to the above example, 0 to 28

nlfib 20 30   will print a range of fibonacci numbers from 20 to 30

AUTHOR
======
Daniel Jenssen <daerandin@gmail.com>

